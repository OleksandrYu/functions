const getSum = (str1, str2) => {
  if (typeof str1 != 'string' || typeof str2 != 'string') {
    return false;
  }
  if (Number.isInteger(Number(str1)) && Number.isInteger(Number(str2))) {
    return (Number(str1) + Number(str2)).toString();
  }
  return false;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let numPosts = 0, numComments = 0;
  for (const post of listOfPosts) {
    if (post.author == authorName) {
      numPosts++;
    }
    if (post.hasOwnProperty("comments")) {
      for (const comment of post.comments) {
        if (comment.author == authorName) {
          numComments++;
        }
      }
    }
  }
  return "Post:" + numPosts + ",comments:" + numComments;
};

const tickets = (people) => {
  let change = 0;
  for (const money of people) {
    if (Number(money) - 25 > change) {
      return "NO"
    } else {
      change += Number(money)
      change -= Number(money) - 25
    }
  }
  return "YES";
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
